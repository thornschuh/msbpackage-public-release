#include <NewSoftSerial.h>
#include <MSBPackage.h>




MSBProcess myMSB(4);  // Benutze Pin 4 als MSB-Daten I/O


MSBPackage P1(1,3,0);  // Sensor Klasse 1 (Voltage) / Adresse 3 / Startwert 0
MSBPackage P2(1,5,0); // Sensor Klasse 1 (Voltage) / Adresse 5 / Startwert 0


/* Callback-Routine die von der MSBProcess Klasse aufgerufen wird um das MSBPackage Objekt für 
   eine bestimme Adresse zu ermitteln. Eine Callback Routinen können in der Regel nach dem gleichen
   Schema aufgebaut werden. Die HanldeRequest Routine muss einfach für die übergebene MSB Adresse (Parameter msbAdr)
   das  einen Zeiger auf das entsprechende MSBPackage Objekt zurückliefern. 

*/
MSBPackage* HandleRequest(byte msbAdr)
{
  if (msbAdr==P1.getAddress())
    return &P1;
  else if (msbAdr==P2.getAddress()) 
    return &P2;
  else  
    return NULL;
}    


/* 
Diese Funktion misst die Zeit in Microsekunden für einen Durchlauf der loop() Funktion und gibt sie auf der seriellen Schnittstelle aus.
Grund für diese Messung - siehe unten
*/

void LoopTimeMeasure()
{
unsigned long Time;

  Time=micros();
  loop(); // loop once
  Time=micros()-Time;
  Serial.print("Looptime: ");Serial.println(Time);
}  

void setup()  
{
  
  
  Serial.begin(115200); // Für Debugausgaben - nicht vergessen den Serial Monitor auch umzuschalten

  myMSB.onRequest(HandleRequest); // Request Handler Funktion (siehe oben) registrieren
  /*
  Aus Performance Gründen wird die HandleRequest Funktion nur für MSB-Adressen, die auf "aktiv" geschaltet sind,
  aufgerufen. Dies erledigt die Methode setAddressActvie 
  */
  myMSB.setAddressActive(P1.getAddress()); // Addresse von MSB-Package P1 aktivieren
  myMSB.setAddressActive(P2.getAddress()); // Addresse von MSB-Package P2 aktivieren

  
  
  analogReference(DEFAULT); // Messbereich des A/D Wandlers auf  0..Vcc (5V) setzen
  
  LoopTimeMeasure(); // Messung zu Testzwecken 
  
 /* Die MSBProcess Klasse übernimmt die Steuerung der loop-Funktion des Sketeches. Daher wird der Methode 
     StartLoop ein Verweis auf die loop()-Funktion des Sketches übergeben. 
  */ 
  myMSB.StartLoop(loop);
   
  
}

/*
Die Funktion ReadAndScaleAnalog hat dient hier nur dazu die Werte der Arduino A/D Wandler auf den Messbereich
0...5V zu skalieren. 
*/

int ReadAndScaleAnalog(int port)
{
long rawValue;  // Als long deklariert damit in der Formel unten eine 32Bit Multipilaktion verwendet wird um Überläufe zu vermeiden 
const long MSBVoltageRange=50L; // A/D Wandler Range ist 0.5V, auf dem MSB entspricht das dem Wert 50 (0,1V Auflösung)
const int ADMax=1023;  // Der A/D Wandler hat eine Auflösung von 10 Bit - daher max. Wert 1023  

  rawValue=analogRead(port);  
//  Serial.println(rawValue);
  return int(rawValue*MSBVoltageRange / ADMax);
  
}


/* 
Die MSBProcess Klasse ruft die loop Funktion immer dann auf wenn Zeit ist. Zur Vermeidung von Problemen mit Reentrance und "Race Conditions"
wird die loop Funktion nicht unterbrochen wenn ein MSB-Adresstoken gesendet wurden. Es wird mit dem Senden des MSB Paketes daher gewartet, bis 
die Loop Funktion zurückkehrt. Falls die Loop Funktion zu lange braucht wird der Sendevorgang verworfen. Um also zu vermeiden dass diese
ständig passiert muss die Loop Funktion eine entsprechend kurze Laufzeit haben. Idealerweise sollte die Laufzeit kürzer als 500us sein.
*/

void loop()
{
int s;  
      
    s=ReadAndScaleAnalog(0);  
    P1.setValue(s); 
    
    s=ReadAndScaleAnalog(1);  
    P2.setValue(s);    
    
}  
