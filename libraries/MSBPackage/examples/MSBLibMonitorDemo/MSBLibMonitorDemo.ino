
#include <NewSoftSerial.h>
#include <MSBPackage.h>


MSBProcess myMSB(4); // Benutze Digital Pin 4 für die Verbindung zum MSB




void setup()  
{
  Serial.begin(57600); // Nicht vergessen die Serial Monitor auch auf diese Geschwindigkeit umzustellen.
  
}



void monitor()                    
{

MSBPackage P;  
char Buffer[10];
float value;
boolean inLimit;


   myMSB.ReadPackage(P);
   
   if (P.isValid()) {
     // Immer bei Adresse 0 eine Linie ausgeben - dient nur der Übersicht
     if (P.getAddress()==0) { Serial.println("-------");  }
     // Nun mit der GetDisplayValue Methode die Werte für die Ausgabe aufbereiten und ausgeben
     if (P.getDisplayValue(Buffer,sizeof(Buffer),value,inLimit)) {
       Serial.print(int(P.getAddress())); Serial.print(": ");
       inLimit?Serial.print(value):Serial.print("-.-");
       Serial.println(Buffer);
      
     }  
   }
}


void loop()
{
  monitor();
}  



