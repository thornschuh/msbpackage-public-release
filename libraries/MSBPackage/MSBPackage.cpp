 /** MSBPackage Library for interfacing with the Multiplex Sensor Bus (MSB)
    Copyright (C) 2011,2012  Thomas Hornschuh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    The Library is published with permission from Multiplex Modellsport Gmbh, Bretten, Germany,
    named "MULTIPLEX" below.

    On request of MULTIPLEX the following statement is added:

    MULTIPLEX strongly recommends to request the official MSB specification when developing
    hard- and software for the Multiplex Sensor Bus. Once requested, MULTIPLEX will automatically
    send updates of the specification to the requestor. It is recommended to check all hard- and software
    against the newest version of the specification before publishing it.

    For devices wich are compatible with the MULTIPLEX Sensor Bus it is possible to use MSBLogo,
    after consultation with MULTIPLEX. The MSB logo is trademarked by MULTIPLEX, so it is
    necessary to get a written permission.
    After permission it is possible to use long and/or short version of MSB-logo.
*/

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/atomic.h>
#include "pins_arduino.h"


#include "MSBPackage.h"


// #define DEBUG_LEVEL 3 // Highest Debug Level

//06.04.2015: The prog_char type is depreciated in newer avr-gcc versions
// For this reason a local type l_prog_char is defined and replaces prog_char 
typedef  const char l_prog_char;

// Marco because I'm lazy :-)
#define PMStr(name,value)  l_prog_char name[] PROGMEM = value

PMStr(dummy,"");
PMStr(voltage," V");
PMStr(current," A");
PMStr(vario," m/s");
PMStr(xspeed," km/h");
PMStr(revolution," 1/min");
PMStr(temperature," degree C");
PMStr(heading," degree");
PMStr(shortdistance," m");
PMStr(level," % Tank");
PMStr(lqi," %LQI");
PMStr(charge," mAh");
PMStr(volume," mL");
PMStr(longdistance," km");

typedef struct _MSBClassTable {
  l_prog_char *unitString;
  float unitFactor;
  int minValue;
  int maxValue;
} MSBClassTable;

const MSBClassTable PROGMEM gMSBClassTable[]  =
{
//  UnitString, UnitFactor, minValue, maxValue
  { dummy,          0,         0,         0       },
  { voltage,        0.1,      -600,       600     },
  { current,        0.1,      -1000,      1000    },
  { vario,          0.1,      -500,       500     },
  { xspeed,         0.1,       0,         6000    },
  { revolution,     100.0,     0,         500     },
  { temperature,    0.1,      -250,       7000    },
  { heading,        0.1,       0,         3600    },
  { shortdistance,  1.0,      -500,       2000    },
  { level,          1.0,        0,        100     },
  { lqi,            1.0,      - 0,        100     },
  { charge,         1.0,      -16000,     16000   },
  { volume,         1.0,      -0,         16000   },
  { longdistance,   0.1,       0,         16000   },
};

const int minClass = 1;
const int maxClass = 13;


void MSBPackage::setValue(int value)
{
int rawValue;

  rawValue = (value * 2) & 0x0fffe;  // Shift left with sign preservation and mask alarm bit

  lowByte = (byte)(rawValue & 0x0ff);
  highByte = (byte)((rawValue & 0xff00) >> 8);

}

MSBPackage::MSBPackage(byte msbClass,byte msbAddress,int value)

{
  Header = ((msbAddress & 0x0f) << 4) | (msbClass & 0x0f);
  setValue(value);
}




boolean MSBPackage::getDisplayValue(char *unitDescription,int bufSize,float &displayValue, boolean &limitOK)
{
MSBClassTable lMSBClass;
int index;
int iValue;

   index=getClass();
   if (isValid() && index>=minClass && index <= maxClass) {
     memcpy_P(&lMSBClass,&gMSBClassTable[index],sizeof(lMSBClass));
     strlcpy_P(unitDescription,lMSBClass.unitString,bufSize);

     iValue=getValue();
     // Check limits
     limitOK = (iValue >= lMSBClass.minValue) && (iValue <=lMSBClass.maxValue);

     if (limitOK)
       displayValue= float(iValue) * lMSBClass.unitFactor;
     else
       displayValue= 0.0;

     return true;
   }
   else
     return false;

}


int MSBPackage::getValue()
{
unsigned int rawValue;

   if (isValid()) {
     rawValue = (unsigned int)lowByte | (unsigned int)highByte << 8;
     rawValue &= 0xfffe; // Mask Alarm flag
     return int(rawValue) / 2; // div by 2 is effectivly a shift right with sign preservation
   } else
     return int(0x8000);
}


boolean MSBPackage::getAlarm()
{
  if (isValid())
    return (lowByte & 0x01) == byte(0x01);
  else
    return false;
}


MSBPackage 		                *MSBProcess::pendingPackage=NULL; // Package to be send next
volatile byte     		        MSBProcess::machineState=sNull; // curent state of the send state machine
volatile unsigned long        	MSBProcess::recvTime=0L;
boolean 		                MSBProcess::sentSuccess=false;
volatile uint16_t               MSBProcess::addressBitmap=0;  // Bitmap for all 16 MSB addresses, 1=respond to address, LSB <> 0x0, MSB <> 0x0f
volatile byte                   MSBProcess::currentToken=0x0ff;
tRequestHandler                 MSBProcess::requestHandler=NULL;


byte MSBProcess::WaitForToken()
// Synchronizes with the MSB. Every MSB transaction starts with an address token (0x0..0x0f) followed by a
// idle time between 256 and 560 microseconds.
{
byte msbByte, nextByte;



#if DEBUG_LEVEL == 3
  Serial.println("WaitForToken call");
#endif
  do
  {
    WaitNext(); // Wait until a byte is available
#if DEBUG_LEVEL == 3
  Serial.println("After WaitNext in WaitForToken call");
#endif
    msbByte= (byte)read();
#if DEBUG_LEVEL == 3
    Serial.print("::");Serial.println(msbByte,HEX);
#endif
    if (msbByte >= 0x0f) continue; // Tokens can only be 0x0..0x0f

    WaitNext();
    nextByte=(byte)peek(); // Only look aheaed - dont read it....
    if ( (nextByte >> 4) == msbByte )  // If next byte is the package header for the token
    {
      // Success
       return msbByte;
    }
      // Otherwise we will continue waiting
  } while (true);
}




boolean MSBProcess::ReadPackage(MSBPackage &Package)
{
byte token;

#if DEBUG_LEVEL == 3
  Serial.println("ReadPackage call");
#endif

  token=WaitForToken();
/// WaitForToken only returns when a byte is available in the serial Buffer
/// So when can direcly read the header

   Package.Header=read();
// Check address == token
   if ( (Package.Header >> 4) == token) {
     WaitNext();
     Package.lowByte=read();
     WaitNext();
     Package.highByte=read();
#if DEBUG_LEVEL == 3
    Serial.print(Package.Header,HEX);Serial.print(":");Serial.print(Package.lowByte,HEX);Serial.print(":");
     Serial.println(Package.highByte,HEX);
#endif
    return true;
   } else {
#if DEBUG_LEVEL == 3
   Serial.print("E:"); Serial.print(Package.Header,HEX); Serial.print("!="); Serial.println(token,HEX);
#endif
   return false;
   }

}

static unsigned int waitcounter=0;
static unsigned long idleCounter=0;
static unsigned long lastTimeDelta=0;



/// Callback method
void MSBProcess::__ReceiveByteCallback(NewSoftSerial *p,char b)
{



  switch (machineState) {

    case tokenReceived:
      if (getTimeDelta() < IdleLineMinTime) {  /// Next Byte receive before idle line minimum
         /// Then it was not a token - so set back to start state
         machineState=sNull;
         currentToken=0x0ff;
         recvTime=0;
      }
      /// Fall thru - because received byte could be the next token itself....
    case sNull:
      if (uint8_t(b)<=0x0f) {
        machineState=tokenReceived;
        currentToken=b;
        recvTime= micros(); // record time of receive
        waitcounter=0;
      }
      break;
    default: ; /// to avoid compiler warnings
  }

}


void MSBProcess::_sendPackage()
{
static unsigned int failures=0;

  lastTimeDelta=getTimeDelta(); // Check idle time again

  if ( lastTimeDelta<IdleLineMaxTime &&  // check if we are to slow....
       machineState==idleLineSuccess && pendingPackage!=NULL) { // safety check...

     ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        Print::write((const uint8_t*)pendingPackage,sizeof(MSBPackage));
      }


      Serial.println(pendingPackage->Header,HEX);//Serial.print(" ");
 //   Serial.print(idleCounter);Serial.print(" "); Serial.println(waitcounter);
 //   Serial.println(lastTimeDelta);

    sentSuccess=true;
  } else {
    sentSuccess=false;
    failures++;
    Serial.print("F::::");Serial.println(failures);
  }
  machineState=sNull;
  idleCounter=0;
  sentSuccess=true;
  currentToken=0x0ff;
}



void MSBProcess::idleLoop()
{
  /// Default implementation does nothing
}


/// Start Main Loop. There are two modes of operation:
/// In "Sketch Mode" the Caller will pass a ptr to a Loop function in a sketch - this is usually the standard loop function
/// In C++ mode (activated with passing NULL to the Loop ptr the IdleLoop method is called - the sketch need to subclass
/// MBSProcess and overload IdleLoop with some usefull information

void MSBProcess::StartLoop(tLoopFPtr LoopFPtr) // Call to start process run
{
int bufferedBytes;

   flush(); // Clear recv. Buffer
   RegisterCallback(MSBProcess::__ReceiveByteCallback);
   do {
     switch (machineState) {
       case sNull:
///        just looping and give other activities a chance
          idleCounter++;

          if (LoopFPtr!=NULL)
             LoopFPtr();
          else
            idleLoop();
          break;
       case tokenReceived:
///       when in tokenReceived state wait for IdleLineMinTime to pass by
///       then request package from main application and send
         waitcounter++;

            if (getTimeDelta()>=IdleLineMinTime) {
                /// minimum idle time reached
                ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                    bufferedBytes=available(); /// number of bytes received until last idle line timeout
                    flush(); // clear received bytes
                    if (bufferedBytes==1 && isAddressActive(currentToken)) {
                        machineState=idleLineSuccess;
                        pendingPackage = requestHandler?requestHandler(currentToken):NULL;
                        _sendPackage();
                    }
                    else  {
//                        Serial.print(currentToken,HEX);
//                        Serial.print("->");
//                        Serial.println(bufferedBytes);
                        machineState=sNull;
                    }
                }
            }
            break;
      default: ; /// To avoid warnings
     }
   } while (true);
}


 void MSBProcess::init()
 {
   begin(MSBBaud);

 }
