 /** MSBPackage Library for interfacing with the Multiplex Sensor Bus (MSB)
    Copyright (C) 2011,2012  Thomas Hornschuh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    On request of MULTIPLEX the following statement is added:

    MULTIPLEX strongly recommends to request the official MSB specification when developing
    hard- and software for the Multiplex Sensor Bus. Once requested, MULTIPLEX will automatically
    send updates of the specification to the requestor. It is recommended to check all hard- and software
    against the newest version of the specification before publishing it.

    For devices wich are compatible with the MULTIPLEX Sensor Bus it is possible to use MSBLogo,
    after consultation with MULTIPLEX. The MSB logo is trademarked by MULTIPLEX, so it is
    necessary to get a written permission.
    After permission it is possible to use long and/or short version of MSB-logo.

    Change log

    05.07.2012: Added method setAlarm to MSBPackage class
    06.04.2015: Fixes in msbpackage.cpp because the prog_char type is depreciated in newer avr-gcc versions
*/


#ifndef __MSBPACKAGE_H
#define __MSBPACKAGE_H

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

// Keine Ahnung ob das noch schöner geht....
#include "../NewSoftSerial/NewSoftSerial.h"

const unsigned int IdleLineMinTime = 256; // MSB minimum idle time for the "idle line multiprocessor protocol"
const unsigned int IdleLineMaxTime = 560; // Max. allowed idle time - if this is exceeded we are "to late"
const long MSBBaud =38400;



typedef enum _States { sNull,           // Start/End Status
                       tokenReceived,   // A candidate for a address token (0x0..0x0f) was received
                       idleLineSuccess, // after tokenReceived the line was idle for at least IdleLineMinTime microseconds
                       sendReady,       // a MSB Package is ready to send and bus is also free to send
                     } MSBStates;



class MSBPackage {
friend class MSBProcess;

  private:
    byte Header;
    byte lowByte;
    byte highByte;

  public:

    inline MSBPackage() {  Header=0; }
    MSBPackage(byte msbClass,byte msbAddress,int value);

// Access methods
    inline byte getHeader() { return Header; }

    inline byte getClass() { return Header & 0x0f; }
    inline byte getAddress() { return Header >> 4; }
    inline boolean isValid() { return getClass() != 0; }
    int getValue();
    void setValue(int value);
    boolean getAlarm();
    inline void setAlarm(boolean bV) { lowByte = (bV)? lowByte | 0x01 : lowByte & 0x0fe;  }
    boolean getDisplayValue(char *unitDescription,int bufSize,float &displayValue, boolean &limitOK);

};


typedef void (*tLoopFPtr)(); /// Pointer to standard Loop() function in Arduino sketch main
typedef MSBPackage* (*tRequestHandler)(byte address);

class MSBProcess : public NewSoftSerial {
/// State Machine Implementation for the MSB Protocol
  private:
     void _sendPackage(); // Internal Package sending

  protected:
/// All communication processing is implemented with static members/methods to simplify implementation
/// because effectivly there will only be one instance of the MSB

    static MSBPackage *pendingPackage; /// Package to be send next
    static volatile byte machineState; /// curent state of the send state machine
    static volatile unsigned long recvTime; /// Time in microseconds of last receive
    static boolean sentSuccess;    /// Flag: Last package sucessfully sent

    static volatile uint16_t addressBitmap;  /// Bitmap for all 16 MSB addresses, 1=respond to address, LSB <> 0x0, MSB <> 0x0f
    static volatile byte currentToken;      /// current Address token
    static tRequestHandler requestHandler; ///  Pointer to request handling function
    static inline boolean _isAddressActive(byte msbAdr) { return (msbAdr<=0x0f)? (addressBitmap & (1<<msbAdr))!=0:false; }

    static void __ReceiveByteCallback(NewSoftSerial *p,char b);

    inline void WaitNext() { while (!available()) ; }


    static inline unsigned long getTimeDelta() {  return micros()-recvTime; }

    virtual void idleLoop();  /// IdleLoop method can be overloaded by subclasses.

    void init();
  public:
     byte WaitForToken();

     void StartLoop(tLoopFPtr LoopFPtr = NULL); /// Call to start process run

     boolean ReadPackage(MSBPackage &Package); /// read next package, return false when error


     inline boolean isSent() { return sentSuccess; }
     inline boolean isSendBusy() { return machineState!=sNull; }
     inline void setAddressActive(byte msbAdr) { if (msbAdr<=0x0f)  addressBitmap |= 1 << msbAdr; }
     inline void setAddressPassive(byte msbAdr) { if (msbAdr<=0x0f) addressBitmap &= ~(1 << msbAdr); }
     inline void resetAllAddresses() { addressBitmap=0; }
     inline boolean isAddressActive(byte msbAdr) { return _isAddressActive(msbAdr); }
     inline void onRequest(tRequestHandler rh) { requestHandler=rh; }

     MSBProcess(uint8_t receivePin, uint8_t transmitPin, bool inverse_logic = false):NewSoftSerial(receivePin,transmitPin,inverse_logic)
     {
        init();
     }

///   New constructor for single pin mode
     MSBProcess(uint8_t txrxPin):NewSoftSerial(txrxPin)
     {
        init();
     }
};


#endif
