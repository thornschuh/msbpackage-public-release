#MSBPackage Library #

##Release Notes ##

27.02.2012 In MSBPackage.cpp und MSBPackage.h Hinweis auf die MSB Spezifikation und die Bedingungen zur Benutzung des MSB Logos eingefügt. Diese Hinweis erfolgte auf Wunsch der Firma Multiplex.

27.02.2012 Timing Parameter für 16Mhz Taktfrequenz in NewSoftSerial.cpp angepasst. Dies wurde erforderlich, da nach den letzten Änderungen am Code scheinbar die Intrabit und Stoppbit Wartezeit zu lang war. Warum das so ist, ist noch etwas rätselhaft – auf jeden Fall was so auf meinem Ardunio Uno Board die Funktion wieder hergestellt. Ich hoffe nun auf Erfahrungsberichte anderer Anwender.

27.02.2012 Dokumentation um die Kapitel „Beispielprogramme“ und „Hinweis von Multiplex“ erweitert.

## Hinweis von Multiplex ##

Der folgende Text wurde auf Wunsch der Firma Multiplex hier aufgenommen: 
Bei Erweiterungen der Library oder Verwendung in kommerziellen Anwendungen ist es sinnvoll, die MSB Spezifikation offiziell bei Multiplex anzufordern, denn nur so hat man die Chance mit seiner Software aktuell zu bleiben.

Für Geräte, welche kompatibel zum MULTIPLEX Sensor Bus sind kann nach Rücksprache mit der Firma MULTIPLEX das MSB-Logo benutzt werden. Da das Logo von MULTIPLEX geschützt ist, ist hierfür die schriftliche Genehmigung der Firma MULTIPLEX erforderlich. Das Logo kann mit der Genehmigung sowohl in langer, als auch in kurzer Form benutzt werden.